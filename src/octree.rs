use std::fmt::*;

use super::space::*;
use super::vector::*;

/// The octree itself.
///
/// Contains outer bounding box (`volume`), and a buffer of `nodes`.
///
/// Nodes don't have bounding boxes; instead, it is recalculated on the go.
///
/// I can shove the boxes into the tree, if profiling shows it is a bottleneck.
///
#[derive(Debug)]
pub struct Octree<A> {
    volume: Cube,
    nodes: Heap<Option<A>>,
}

impl<A: Copy + Debug> Octree<A> {
    /// Add a leaf node.
    ///
    fn leaf(&mut self, content: Option<A>) -> usize {
        self.nodes.alloc(content, None)
    }

    /// Create a tree with given bounding box.
    ///
    /// The volume is filled with vacuum.
    ///
    pub fn new(cube: Cube) -> Self {
        let mut nodes = Heap::new();
        nodes.alloc(None, None);
        Octree {
            nodes,
            volume: cube,
        }
    }

    /// Retrieve a node from the tree at point. Will not descent into homogeneous
    /// cubes of matter.
    ///
    pub fn get(&self, point: Point) -> Option<A> {
        let mut node = self.nodes.get(0);
        let mut cube = self.volume;
        loop {
            if let Some(children) = node.refs {
                let octant = cube.octant(point);
                cube = cube.descent(octant);
                node = self.nodes.get(children[octant])
            } else {
                return node.content;
            }
        }
    }

    /// Make sure the node is Branch; otherwise, covert to branch.
    ///
    fn ensure_split(&mut self, ix: usize) -> [usize; 8] {
        let mut cell = self.nodes.get(ix);
        if let Some(children) = cell.refs {
            children
        } else {
            let mut children = [0; 8];
            for child in &mut children {
                *child = self.leaf(cell.content);
            }
            cell.refs = Some(children);
            self.nodes.set(ix, cell);
            children
        }
    }

    /// Set cube with given radius in the tree. 0 is 1^3, 1 is 2^3, etc.
    ///
    /// Will set a cube snapped to the recursive grid. Useful for loading models.
    ///
    /// Will descend into and shatter the homogeneous matter, until radius is reached.
    ///
    pub fn set_cube(&mut self, point: Point, a: Option<A>, log_radius: u32) {
        let mut current = 0;
        let mut cube = self.volume;
        let radius = 2_usize.pow(log_radius);
        loop {
            if radius <= cube.radius {
                let children = self.ensure_split(current);
                let octant = cube.octant(point);
                current = children[octant];
                cube = cube.descent(octant)
            } else {
                self.nodes.dealloc_children(current);
                let mut cell = self.nodes.get(current);
                cell.content = a;
                cell.refs = None;
                self.nodes.set(current, cell);
                return;
            }
            // println!("-");
        }
    }

    /// Set a point in the tree.
    ///
    /// Will descend into and shatter the homogeneous matter.
    ///
    pub fn set(&mut self, point: Point, a: Option<A>) {
        self.set_cube(point, a, 0)
    }
}
