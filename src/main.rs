mod octree;
mod space;
mod vector;
use octree::*;
use space::*;

fn main() {
    // let mut vector = Heap::new();

    // vector.alloc(1, None);
    // vector.alloc(2, None);
    // vector.alloc(3, None);
    // vector.alloc(4, None);
    // vector.alloc(5, None);
    // vector.alloc(6, None);
    // vector.alloc(7, None);
    // vector.alloc(8, None);
    // let nine = vector.alloc(9, Some([0, 1, 2, 3, 4, 5, 6, 7]));
    // vector.dealloc(nine);
    // vector.alloc(0, None);

    // // vector.dealloc(five);

    // // vector.shriek();

    // vector.print();

    let cube = Cube::new(Point { x: 0, y: 0, z: 0 }, 16);

    let mut tree = Octree::<Point>::new(cube);

    for x in 0..=255 {
        for y in 0..=255 {
            for z in 0..=255 {
                tree.set(Point { x, y, z }, Some(Point { x, y, z }));
            }
        }
    }

    println!("done writing");

    tree.set_cube(Point { x: 0, y: 0, z: 0 }, None, 16);

    println!("done deleting");

    for x in 0..=255 {
        for y in 0..=255 {
            for z in 0..=255 {
                tree.set(Point { x, y, z }, Some(Point { x, y, z }));
            }
        }
    }

    println!("done writing again");

    tree.set(Point { x: 3, y: 4, z: 5 }, Some(Point { x: 1, y: 2, z: 3 }));

    // for z in 0..=255 {
    //     for x in 0..=255 {
    //         for y in 0..=255 {
    //             let pt = tree.get(Point { x, y, z });
    //             if pt != Some(Point { x, y, z }) {
    //                 println!("found {:?} at {:?}", pt, Point { x, y, z })
    //             } else {
    //                 // print!(".")
    //             }
    //         }
    //     }
    // }
    println!("{:?}", tree.get(Point { x: 3, y: 4, z: 5 }));
    println!("{:?}", tree.get(Point { x: 3, y: 5, z: 5 }));
}
