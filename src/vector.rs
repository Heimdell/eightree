use std::fmt::Debug;

/// Mutable memory cell.
///
#[derive(Copy, Clone, Debug)]
pub struct Cell<A> {
    pub content: A,               // external content
    pub refs: Option<[usize; 8]>, // subtree refs (yes, we have to know it here)
    next: Option<usize>,          // gc list ref
    free: bool,                   // false if allocated
}

/// Garbage-collected heap.
///
/// The following is assumed and NOT checked (/will/ cause `panic`):
///  1) No element is referred twice - we have tree, not a DAG.
///  2) No dangling references.
///
#[derive(Debug)]
pub struct Heap<A> {
    cells: Vec<Cell<A>>,
    top: Option<usize>,
    void: usize,
}

impl<A: Copy + Debug> Heap<A> {
    /// Create an empty heap.
    ///
    pub fn new() -> Self {
        Heap {
            cells: vec![],
            top: None,
            void: 0,
        }
    }

    /// Allocate an element in the heap.
    ///
    /// Will only (possibly) realloc underlying vector once
    ///  1) no free cells left AND vector has exhausted its capacity
    ///  2) count of free cells exceeds half of the vector (via copying GC)
    ///
    pub fn alloc(&mut self, content: A, refs: Option<[usize; 8]>) -> usize {
        if self.void * 2 > self.cells.len() {
            self.shriek()
        }
        let cell: Cell<A> = Cell {
            content,
            refs,
            next: None,
            free: false,
        };
        if let Some(a) = self.top {
            self.top = self.cells[a].next;
            self.cells[a] = cell;
            self.void -= 1;
            a
        } else {
            self.cells.push(cell);
            self.cells.len() - 1
        }
    }

    /// Deallocate an element in the heap.
    ///
    /// Will dealloc throught all references to other elements, recursively.
    ///
    pub fn dealloc(&mut self, index: usize) {
        if let Some(_) = self.cells[index].next {
            panic!("double dealloc({:?})", index)
        }
        self.dealloc_children(index);
        self.cells[index].next = self.top;
        self.cells[index].free = true;
        self.void += 1;
        self.top = Some(index)
    }

    /// Deallocate all child elements, recursively.
    ///
    /// After that, the `refs` field of the cell must be set to `None`.
    ///
    pub fn dealloc_children(&mut self, index: usize) {
        if let Some(refs) = self.cells[index].refs {
            for child in refs {
                self.dealloc(child)
            }
        }
    }

    /// ShriNks the internal vector, fixing the internal references in the
    ///  process.
    ///
    /// No external reference is guaranteed to point into the same location!
    ///  Except 0-one, which is root node.
    ///
    fn shriek(&mut self) {
        let mut map: Vec<usize> = vec![];
        let mut hp = 0;
        for i in 0..self.cells.len() {
            map.push(hp);
            if !self.cells[i].free {
                hp += 1;
            }
        }
        let mut res = vec![];
        res.reserve_exact(hp); // doesn't make any difference, tbh
        for i in 0..self.cells.len() {
            if let Some(refs) = self.cells[i].refs {
                let mut out = refs;
                for i in 0..8 {
                    out[i] = map[out[i]]
                }
                self.cells[i].refs = Some(out);
            }
            if !self.cells[i].free {
                res.push(self.cells[i])
            }
        }
        // println!("shrieked from {:?} to {:?}", self.cells.len(), res.len());
        self.cells = res;
        self.void = 0;
        self.top = None;
    }

    /// Returns a cell with given index.
    ///
    /// You should not mutate the tree while holding it, if you plan to write it
    ///  back or read its refs.
    ///
    pub fn get(&self, index: usize) -> Cell<A> {
        let cell = self.cells[index];
        if let Some(_) = cell.next {
            panic!("get({:?}) on a free cell", index)
        }
        cell
    }

    /// Writes a cell at given index.
    ///
    pub fn set(&mut self, index: usize, value: Cell<A>) {
        if let Some(_) = self.cells[index].next {
            panic!("set({:?}, ?) on a free cell", index)
        }
        self.cells[index].content = value.content;
        self.cells[index].refs = value.refs;
    }
}
