/// Point in discrete space.
//
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Point {
    pub x: i64,
    pub y: i64,
    pub z: i64,
}

/// A cubic region of discrete space with size of 2^something.
//
#[derive(Copy, Clone, Debug)]
pub struct Cube {
    pub center: Point,
    pub radius: usize,
}

impl Cube {
    /// Make new cube, preserve the 2^smth constraint on radius.
    //
    pub fn new(center: Point, log_radius: u32) -> Cube {
        Cube {
            center,
            radius: 2_usize.pow(log_radius),
        }
    }

    /// Get the octant the point belongs to, relative to the cube center.
    //
    pub fn octant(&self, point: Point) -> usize {
        (if point.x >= self.center.x { 1 } else { 0 })
            + if point.y >= self.center.y { 2 } else { 0 }
            + if point.z >= self.center.z { 4 } else { 0 }
    }

    /// Cut an octant off the cube.
    //
    pub fn descent(&self, octant: usize) -> Self {
        let center = Point {
            x: if octant & 1 == 1 {
                self.center.x + (self.radius / 2) as i64
            } else {
                self.center.x - (self.radius / 2) as i64
            },
            y: if octant & 2 == 2 {
                self.center.y + (self.radius / 2) as i64
            } else {
                self.center.y - (self.radius / 2) as i64
            },
            z: if octant & 4 == 4 {
                self.center.z + (self.radius / 2) as i64
            } else {
                self.center.z - (self.radius / 2) as i64
            },
        };
        Cube {
            center,
            radius: self.radius / 2,
        }
    }
}
